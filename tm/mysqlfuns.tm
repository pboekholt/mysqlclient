\function{mysql_connect}
\synopsis{connect to a mysql database}
\usage{obj mysql_connect(host, user, password)}
\description
  This function connects to a mysql database and returns a mysql
  connection object.
\qualifiers
\qualifier{database}{database to use}
\methods
\method{select_db}{select a database}
\method{query}{Runs a query}
\method{query_struct}{Runs a query and returns a struct}
\method{query_list_of_assoc}{Runs a query and returns a list of assoc}
\method{escape}{escape a mysql query parameter}
\method{prepare}{prepares a server-side prepared statement}
\method{affected_rows}{Get number of affected rows in previous MySQL operation}
\method{insert_id}{Get the ID generated in the last query}
\seealso{dbh.query, dbh.query_struct, dbh.query_list_of_assoc, dbh.prepare}
\done

\function{dbh.query}
\synopsis{query a mysql database}
\usage{dbh.query()}
\description
  Execute a SQL query on a mysql database, without returning a result.
\seealso{mysql_connect, dbh.query_struct, dbh.query_list_of_assoc}
\done

\function{dbh.query_struct}
\synopsis{query a mysql database and return a struct}
\usage{datastruct = dbh.query_struct()}
\description
  Execute a SQL query on a mysql database, and returns the result as
  a structure of arrays of strings.
\notes
  You should make sure that the SQL result column names are valid
  structure fieldnames.
\seealso{mysql_connect, dbh.query, dbh.query_list_of_assoc}
\done

\function{dbh.query_list_of_assoc}
\synopsis{query a mysql database and return a list of assoc}
\usage{list = dbh.query_list_of_assoc()}
\description
  Execute a SQL query on a mysql database, and returns the result as
  a list of assocs of strings.
\seealso{mysql_connect, dbh.query, dbh.query_struct}
\done

\function{dbh.escape}
\synopsis{escape a mysql query parameter}
\usage{string = dbh.escape(string)}
\description
  This function escapes special characters in a string for use as a
  query parameter.
\notes
  This is not needed with prepared statements.
\seealso{mysql_connect, dbh.query, dbh.prepare}
\done

\function{dbh.prepare}
\synopsis{prepare a mysql prepared statement}
\usage{obj = dbh.prepare(String_Type query)}
\description
  This method prepares a server-side prepared statement and returns
  a statement handle.
  
  Question marks in the query are placeholders.
\methods
\method{execute}{execute the statement}
\method{execute_list_of_struct}{execute the statement and return a list of structs}
\method{execute_list_of_assoc}{execute the statement and return a list of assocs}
\method{execute_list_of_list}{execute the statement and return a list of lists}
\seealso{mysql_connect, stmt.execute, stmt.execute_list_of_struct, stmt.execute_list_of_assoc}
\done

\function{dbh.affected_rows}
\synopsis{Get number of affected rows in previous MySQL operation}
\usage{int = dbh.affected_rows()}
\description
  Returns the number of rows changed, deleted, or inserted by the last
  statement.
\seealso{dbh.query, stmt.execute}
\done

\function{dbh.insert_id}
\synopsis{Get the ID generated in the last query}
\usage{int = dbh.insert_id()}
\description
  Returns the value generated for an AUTO_INCREMENT column by the
  previous INSERT or UPDATE statement.
\seealso{dbh.query, stmt.execute}
\done

\function{stmt.execute}
\synopsis{Execute a mysql prepared statement}
\usage{stmt.execute(...)}
\description
  Execute a mysql prepared statement, without returning a result.
  Optional parameters are values for placeholders.
\seealso{dbh.prepare, stmt.execute_list_of_struct, stmt.execute_list_of_assoc}
\done

\function{stmt.execute_list_of_struct}
\synopsis{Execute a mysql prepared statement and return a list of struct}
\usage{list = stmt.execute_list_of_struct(...)}
\description
  Execute a mysql prepared statement, and return the result as a list
  of structs. Optional parameters are values for placeholders.
\notes
  You should make sure that the SQL result column names are valid
  structure fieldnames.
\seealso{dbh.prepare, stmt.execute, stmt.execute_list_of_assoc, stmt.execute_list_of_list}
\done

\function{stmt.execute_list_of_assoc}
\synopsis{Execute a mysql prepared statement and return a list of assoc}
\usage{list = stmt.execute_list_of_assoc(...)}
\description
  Execute a mysql prepared statement, and return the result as a list
  of assocs of Any_Type. Optional parameters are values for
  placeholders.
\notes
  This can be much slower than \sfun{stmt.execute_list_of_struct} or
  \sfun{stmt.execute_list_of_list}
\seealso{dbh.prepare, stmt.execute, stmt.execute_list_of_struct, stmt.execute_list_of_list}
\done

\function{stmt.execute_list_of_list}
\synopsis{Execute a mysql prepared statement and return a list of lists}
\usage{list = stmt.execute_list_of_list(...)}
\description
  Execute a mysql prepared statement, and return the result as a list
  of lists. Optional parameters are values for placeholders.
\seealso{dbh.prepare, stmt.execute, stmt.execute_list_of_struct, stmt.execute_list_of_assoc}
\done
