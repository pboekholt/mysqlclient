#!/usr/bin/env slsh
()=evalfile("./mysqlclient.sl");

% These tests require the TESS module. You can get it from
% http://space.mit.edu/cxc/software/slang/modules/tess/ or on Debian
% you can install the slang-tess package.
%
% To run these tests, you need a database connection. You can
% initialize a dbh object in a file called "tess-common.sl" in the
% current directory.

require("tess");

private define error()
{
   variable args = __pop_list(_NARGS);
   throw RunTimeError, __push_list(args);
}

private define test_execute(query, expected)
{
   variable sth = dbh.prepare(query);
   variable result = sth.execute_list_of_struct();
   result = result[0].col1;
   if (expected != result)
     {
	error("expected $expected, got $result"$);
     }
}

private define test_param(sth, param, expected)
{
   variable result = sth.execute_list_of_struct(param);
   result = result[0].col1;
   if (expected != result)
     {
	error("expected $expected, got $result"$);
     }
}

variable params = {-5000, -200, -1, 0 , 1, 2, 255, 256, 20000, 50000, 100000000,
   -0.5, 0.0, 1.0, 100.001, NULL
};
variable x;
message("execute tests...");
foreach x (params)
{
   tess_invoke(0, &test_execute, "select $x as col1"$, x);
}

message("parameter tests...");
variable sth = dbh.prepare("select ? as col1");
foreach x (params)
{
   tess_invoke(0, &test_param, sth, x, x);
}


% date/datetime

private define test_struct(query, expected)
{
   variable sth = dbh.prepare(query);
   variable result = sth.execute_list_of_struct();
   result = result[0].col1;
   if ((wherefirst(get_struct_field_names(result) != get_struct_field_names(expected)) != NULL)
       || wherefirst([typecast(_push_struct_field_values(result), Integer_Type)]
		     != [typecast(_push_struct_field_values(expected), Integer_Type)]) != NULL)
     {
	error("expected $expected, got $result"$);
     }
}

tess_invoke(0, &test_struct, "select date('2011-11-11') as col1", struct
	    {
	       tm_sec = 0,
	       tm_min = 0,
	       tm_hour = 0,
	       tm_mday = 11,
	       tm_mon = 10,
	       tm_year = 111,
	       tm_wday = 0,
	       tm_yday = 0,
	       tm_isdst = 0
	    }
	   );

tess_invoke(0, &test_struct, "select date('0000-00-00') as col1", struct
	    {
	       tm_sec = 0,
	       tm_min = 0,
	       tm_hour = 0,
	       tm_mday = 0,
	       tm_mon = 0,
	       tm_year = 0,
	       tm_wday = 0,
	       tm_yday = 0,
	       tm_isdst = 0
	    }
);

private define test_struct_param(query, expected, param)
{
   variable sth = dbh.prepare(query);
   variable result = sth.execute_list_of_struct(param);
   result = result[0].col1;
   if ((wherefirst(get_struct_field_names(result) != get_struct_field_names(expected)) != NULL)
       || wherefirst([typecast(_push_struct_field_values(result), Integer_Type)]
		     != [typecast(_push_struct_field_values(expected), Integer_Type)]) != NULL)
     {
	error("expected $expected, got $result"$);
     }
}

tess_invoke(0, &test_struct_param, "select date_add(?, interval 0 second) as col1",
	    struct
	    {
	       tm_sec = 0,
	       tm_min = 1,
	       tm_hour = 1,
	       tm_mday = 11,
	       tm_mon = 10,
	       tm_year = 111,
	       tm_wday = 0,
	       tm_yday = 0,
	       tm_isdst = 0
	    },
	     struct
	    {
	       tm_sec = 0,
	       tm_min = 1,
	       tm_hour = 1,
	       tm_mday = 11,
	       tm_mon = 10,
	       tm_year = 111,
	       tm_wday = 0,
	       tm_yday = 0,
	       tm_isdst = 0
	    });
tess_invoke(0, &test_execute, "select maketime(12,15,30) as col1", 44130);
tess_invoke(0, &test_execute, "select maketime(240,0,0) as col1", 864000);
tess_invoke(0, &test_execute, "select maketime(-240,0,0) as col1", -864000);

private define test_quote(param)
{
   variable query = "select '"B + dbh.escape(param)+ "' as a"B;
   variable result = dbh.query_struct(query).a[0];
   if (result != param)
     {
	error("expected $param, got $result"$);
     }
}
tess_invoke(0, &test_quote, "foo");
tess_invoke(0, &test_quote, "foo'bar");
tess_invoke(0, &test_quote, "fo'o\\bar");
tess_invoke(0, &test_quote, "fo?342/= \0o");

dbh.query("set sql_mode='NO_BACKSLASH_ESCAPES'");
tess_invoke(0, &test_quote, "foo");
tess_invoke(0, &test_quote, "foo'bar");
tess_invoke(0, &test_quote, "fo'o\\bar");

