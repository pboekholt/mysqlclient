% mysqlclient.sl
% 
% Copyright (c) 2011 Paul Boekholt.
% Released under the terms of the GNU GPL (version 2 or later).
% 
% A pure S-Lang MySQL client library. Requires S-Lang 2.2 or greater
% 
% Version: 0.1.0

require("socket");
require("select");
%{{{ constants
%{{{  server capabilities
private variable CLIENT_LONG_PASSWORD = 1,
  CLIENT_FOUND_ROWS = 2,
  CLIENT_LONG_FLAG = 4,
  CLIENT_CONNECT_WITH_DB = 8,
  CLIENT_NO_SCHEMA = 16,
  CLIENT_COMPRESS,
  CLIENT_ODBC = 64,
  CLIENT_LOCAL_FILES = 128,
  CLIENT_IGNORE_SPACE = 256,
  CLIENT_PROTOCOL_41 = 512,
  CLIENT_INTERACTIVE = 1024,
  CLIENT_SSL = 2048,
  CLIENT_IGNORE_SIGPIPE = 4096,
  CLIENT_TRANSACTIONS = 8192,
  CLIENT_RESERVED = 16384,
  CLIENT_SECURE_CONNECTION = 32768,
  CLIENT_MULTI_STATEMENTS = 65536,
  CLIENT_MULTI_RESULTS = 131072;

%}}}
%{{{  commands
private variable COM_SLEEP =  0x00,  %  (none, this is an internal thread state)
  COM_QUIT                 =  0x01,  %  mysql_close
  COM_INIT_DB              =  0x02,  %  mysql_select_db 
  COM_QUERY                =  0x03,  %  mysql_real_query
  COM_FIELD_LIST           =  0x04,  %  mysql_list_fields
  COM_CREATE_DB            =  0x05,  %  mysql_create_db (deprecated)
  COM_DROP_DB              =  0x06,  %  mysql_drop_db (deprecated)
  COM_REFRESH              =  0x07,  %  mysql_refresh
  COM_SHUTDOWN             =  0x08,  %  mysql_shutdown
  COM_STATISTICS           =  0x09,  %  mysql_stat
  COM_PROCESS_INFO         =  0x0a,  %  mysql_list_processes
  COM_CONNECT              =  0x0b,  %  (none, this is an internal thread state)
  COM_PROCESS_KILL         =  0x0c,  %  mysql_kill
  COM_DEBUG                =  0x0d,  %  mysql_dump_debug_info
  COM_PING                 =  0x0e,  %  mysql_ping
  COM_TIME                 =  0x0f,  %  (none, this is an internal thread state)
  COM_DELAYED_INSERT       =  0x10,  %  (none, this is an internal thread state)
  COM_CHANGE_USER          =  0x11,  %  mysql_change_user
  COM_BINLOG_DUMP          =  0x12,  %  sent by the slave IO thread to request a binlog
  COM_TABLE_DUMP           =  0x13,  %  LOAD TABLE ... FROM MASTER (deprecated)
  COM_CONNECT_OUT          =  0x14,  %  (none, this is an internal thread state)
  COM_REGISTER_SLAVE       =  0x15,  %  sent by the slave to register with the master (optional)
  COM_STMT_PREPARE         =  0x16,  %  mysql_stmt_prepare
  COM_STMT_EXECUTE         =  0x17,  %  mysql_stmt_execute
  COM_STMT_CLOSE           =  0x19,  %  mysql_stmt_close
  COM_STMT_RESET           =  0x1a,  %  mysql_stmt_reset
  COM_SET_OPTION           =  0x1b,  %  mysql_set_server_option
  COM_STMT_FETCH           =  0x1c,  %  mysql_stmt_fetch
  COM_STMT_SEND_LONG_DATA  =  0x18;  %  mysql_stmt_send_long_data


%}}}
%{{{  field types
private variable FIELD_TYPE_DECIMAL =  0x00,
  FIELD_TYPE_TINY        =  0x01,
  FIELD_TYPE_SHORT       =  0x02,
  FIELD_TYPE_LONG        =  0x03,
  FIELD_TYPE_FLOAT       =  0x04,
  FIELD_TYPE_DOUBLE      =  0x05,
  FIELD_TYPE_NULL        =  0x06,
  FIELD_TYPE_TIMESTAMP   =  0x07,
  FIELD_TYPE_LONGLONG    =  0x08,
  FIELD_TYPE_INT24       =  0x09,
  FIELD_TYPE_DATE        =  0x0a,
  FIELD_TYPE_TIME        =  0x0b,
  FIELD_TYPE_DATETIME    =  0x0c,
  FIELD_TYPE_YEAR	 =  0x0d,
  FIELD_TYPE_NEWDATE     =  0x0e,
  FIELD_TYPE_VARCHAR     =  0x0f, % (new in MySQL 5.0)
  FIELD_TYPE_BIT         =  0x10, % (new in MySQL 5.0)
  FIELD_TYPE_NEWDECIMAL  =  0xf6, % (new in MYSQL 5.0)
  FIELD_TYPE_ENUM        =  0xf7,
  FIELD_TYPE_SET         =  0xf8,
  FIELD_TYPE_TINY_BLOB   =  0xf9,
  FIELD_TYPE_MEDIUM_BLOB =  0xfa,
  FIELD_TYPE_LONG_BLOB   =  0xfb,
  FIELD_TYPE_BLOB        =  0xfc,
  FIELD_TYPE_VAR_STRING  =  0xfd,
  FIELD_TYPE_STRING      =  0xfe,
  FIELD_TYPE_GEOMETRY    =  0xff;

%}}}
%{{{  flags
private variable NOT_NULL_FLAG	= 1, 	% Field can't be NULL
  PRI_KEY_FLAG	    = 2,	% Field is part of a primary key
  UNIQUE_KEY_FLAG   = 4, 	% Field is part of a unique key
  MULTIPLE_KEY_FLAG = 8, 	% Field is part of a key
  BLOB_FLAG	 = 16, 		% Field is a blob
  UNSIGNED_FLAG	 = 32, 		% Field is unsigned
  ZEROFILL_FLAG	 = 64, 		% Field is zerofill
  BINARY_FLAG	 = 128, 	% Field is binary  
  ENUM_FLAG	 = 256, 	% field is an enum
  AUTO_INCREMENT_FLAG = 512, 	% field is a autoincrement field
  TIMESTAMP_FLAG = 1024, 	% Field is a timestamp
  SET_FLAG	 = 2048, 	% field is a set
  NO_DEFAULT_VALUE_FLAG = 4096, % Field doesn't have default value
  NUM_FLAG	 = 32768, 	% Field is num (for clients)
  PART_KEY_FLAG	 = 16384,  	% Intern; Part of some key
  GROUP_FLAG	 = 32768, 	% Intern: Group field
  UNIQUE_FLAG	 = 65536, 	% Intern: Used by sql_yacc
  BINCMP_FLAG	 = 131072; 	% Intern: Used by sql_yacc

%}}}
%{{{  server status
private variable SERVER_STATUS_NO_BACKSLASH_ESCAPES = 512;
%}}}
%}}}
%{{{ exceptions
new_exception("MysqlError", RunTimeError, "Mysql Error");
new_exception("MysqlServerError", MysqlError, "Mysql Server Error");
new_exception("MysqlDuplicateEntryError", MysqlServerError, "Duplicate Entry Error");
new_exception("MysqlClientError", MysqlError, "Mysql Client Error");
%}}}
%{{{ 4.1 password scrambling
#if (_slang_version < 20300)
private define rotateleft(x,n)
{
   return ((x << n) | (x >> (32-n)));
}

private define SHA1(str1)
{
   variable current_length = bstrlen(str1);
   variable str = [unpack("C${current_length}"$, str1), UChar_Type[100]];

   variable original_length = current_length;
   str[current_length] = 0x80;
   current_length++;

   variable ib = current_length mod 64;
   if(ib<56)
     ib = 56-ib;
   else
     ib = 120 - ib;

   current_length += ib + 6;

   str[current_length] = (original_length * 8) / 0x100 ;
   current_length++;
   str[current_length] = (original_length * 8) mod 0x100;
   current_length++;

   variable number_of_chunks = current_length/64;
   variable word = ULong_Type[80];
   
   variable a,b,c,d,e,f,k;
   variable h = [
		 0x67452301UL,
		 0xEFCDAB89UL,
		 0x98BADCFEUL,
		 0x10325476UL,
		 0xC3D2E1F0UL
		];
   variable i, j, m;
   USER_BLOCK0
     {
	(a, e, d, c, b) = ((rotateleft(a,5) + f + e + k + word[m]) & 0xFFFFFFFF,
			   d, c, rotateleft(b, 30), a);
     }
   
   _for i (0, number_of_chunks - 1, 1)
     {
	_for j (0, 15, 1)
	  {
	     word[j] = str[i*64 + j*4 + 0] * 0x1000000
	       + str[i*64 + j*4 + 1] * 0x10000
	       + str[i*64 + j*4 + 2] * 0x100
	       + str[i*64 + j*4 + 3];
	  }
	  
	_for j (16, 79, 1)
	  {
	     word[j] = rotateleft((word[j-3] xor word[j-8] xor word[j-14] xor word[j-16]),1);
	  }
	
	a = h[0];
	b = h[1];
	c = h[2];
	d = h[3];
	e = h[4];

	k = 0x5A827999;
	_for m (0, 19, 1)
	  {
	     f = (b & c) | ((~b) & d);
	     X_USER_BLOCK0;
	  }
	k = 0x6ED9EBA1;
	_for m (20, 39, 1)
	  {
	     f = b xor c xor d;
	     X_USER_BLOCK0;
	  }
	k = 0x8F1BBCDC;
	_for m (40, 59, 1)
	  {
	     f = (b & c) | (b & d) | (c & d);
	     X_USER_BLOCK0;
	  }
	k = 0xCA62C1D6;
	_for m (60, 79, 1)
	  {
	     f = b xor c xor d;
	     X_USER_BLOCK0;
	  }

	h += [a, b, c, d, e];
     }
   return pack(">K5", h);
}
#else
require("chksum");

private define SHA1(s)
{
   s = bstring_to_array(sha1sum(s));
   s = s - 48 - 39 * ((s >> 6) & 1);
   return pack("C20", s[[0:38:2]] * 16 + s[[1:39:2]]);
}
#endif

private define scramble41(password, seed)
{
   variable s1 = SHA1(password);
   return pack("C20", bstring_to_array(s1) xor bstring_to_array(SHA1(seed + SHA1(s1))));
}
%}}}
%{{{ socket/packet functions
private define read_n_bytes(s, n)
{
   variable len, more, buf;
   len = s.bufferlen - s.offset;
   if (len > n)
     {
	s.buffer[[s.offset : s.offset + n - 1]];
	s.offset += n;
	return;
     }
   else if (len == n)
     {
	s.buffer[[s.offset : s.offset + n - 1]];
	s.buffer = ""B;
	s.offset = 0;
	s.bufferlen = 0;
	return;
     }
   variable rv = s.buffer[[s.offset :]];
   n -= len; % n bytes left to read

   % The maximum packet length is 16MB = 16777216 bytes. This should
   % fetch at most 5000 * 4096 = 20480000 bytes, so it should usually
   % work.

   loop(5000)
     {
	more = select([s.socket], NULL, NULL, 10);
	ifnot (more.nready)
	  {
	     sleep(0.05);
	     more = select([s.socket], NULL, NULL, 10);
	  }
	ifnot (more.nready)
	  {
	     throw IOError, "server not responding";
	  }
	len = read(s.socket, &buf, 4096);
	if (len == -1)
	  {
	     throw IOError, "error reading from server";
	  }
	else if (len <= n)
	  {
	     rv += buf;
	     n -= len;
	     if (n == 0)
	       {
		  s.buffer = ""B;
		  s.offset = 0;
		  s.bufferlen = 0;
		  return rv;
	       }
	  }
	else % len > n
	  {
	     s.buffer = buf;
	     s.offset = n;
	     s.bufferlen = len;
	     rv += buf[[: n-1]];
	     return rv;
	  }
     }
   then
     {
	throw IOError, "could not read server response";
     }
}

private define read_packet(s)
{
   % The first three bytes are the packet length.
   % The fourth is the packet number.
   % We ignore the packet number, so we just read four bytes
   % and AND it with 2^24 - 1
   variable packet = read_n_bytes(s, unpack("<K", read_n_bytes(s, 4)) & 16777215);
   if (packet[0] == 255)
     {
	variable exception, obj = struct { sqlstate, errno, error };
	(obj.errno, obj.sqlstate) = unpack("x<hxs5", packet);
	obj.error = packet[[9:]];
	switch (obj.errno)
	  {
	   case 1062:
	     exception = MysqlDuplicateEntryError;
	  }
	  {
	     exception = MysqlServerError;
	  }
	throw exception, obj.error, obj;
     }
   if (qualifier_exists("expect") && packet[0] != qualifier("expect")) 
     {
	throw MysqlError, sprintf("expected %d code, got %d", qualifier("expect"), packet[0]);
     }
   return packet;
}

private define read_eof_packet(s)
{
   () = read_packet(s; expect = 0xfe);
}

private define read_up_to_eof(s)
{
   variable response;
   forever
     {
	response = read_packet(s);
	if (response[0] == 0xfe)
	  {
	     break;
	  }
     }
}

private define write_packet(s, b)
{
   variable packetlen = bstrlen(b);
   ()=write(s.socket, pack("<J x C s${packetlen}"$, packetlen, qualifier("sequence", 0), b));
}

private define mysql_socket_new(host, port)
{
   variable s = socket(PF_INET, SOCK_STREAM, 0);
   connect(s, host, port);
   return struct {offset = 0, bufferlen = 0, socket=s, buffer= ""B};
}

%}}}
%{{{ element encoding functions

% parse a length coded binary.
% arguments:
%  a bstring
%  an index into the bstring
%  optionally a reference to store the length in bytes of the binary
% returns:
%  a number, or (if the binary is the length of a row packet
%  field with a NULL value) NULL
private define parse_lcb()
{
   variable s, i, l = NULL;
   if (_NARGS == 3)
     {
	(s,i,l) = ();
     }
   else
     {
	(s,i) = ();
     }
   variable len, number = s[i];
   (number, len) = number < 251 ? (number, 1) :
     number == 251 ? (NULL, 1) :
     number == 252 ? (s[i+1] + 256 * s[i+2], 3) :
     number == 253 ? (s[i+1] + 256 * s[i+2] + 65536 * s[i+3], 4) :
     (unpack("<K", s[[i+1:i+4]]), 5);
   if (l != NULL)
     @l = len;
   return number;
}

private define lcb(num)
{
   if (num == NULL) return "\xfb"B;
   if (num < 251) return pack("C", num);
   if (num < 65536) return pack("C <J", 252, num);
   if (num < 16777216) return pack("C <J C", 253, num & 0xffff, num >> 16);
   return pack("C <J <J", 254, num & 0xffffffff, num >> 32);
}

% parse a length coded string
private define parse_lcs()
{
   variable s, i, l, len, blen;
   if (_NARGS == 3)
     {
	(s,i,l) = ();
	len = parse_lcb(s, i, &blen);
	@l = len == NULL ? blen : len + blen;
     }
   else
     {
	(s,i) = ();
	len = parse_lcb(s, i, &blen);
     }
   return len == NULL ? NULL : len > 0 ? s[[i+blen:i+blen+len-1]] : ""B;
}

private define lcs(s)
{
   return lcb(bstrlen(s)) + s;
}

%}}}
%{{{ helper functions

private define parse_field_packet(response)
{
   variable names = String_Type[6], i = 0, k;
   variable field = struct
     {
	catalog, db, table, org_table, name, org_name,
	charsetnr, length, type, flags, decimals
     };
   _for k (0, 5, 1)
     {
	variable len = response[i];
	if (len > 250)
	  {
	     throw MysqlClientError, "identifiers > 250 bytes are not supported";
	  }
	
	names[k]=response[[i+1:i+len]];
	i += len + 1;
     }
   
   % i is now a 0-based index to the first byte after org_name
   % add 1 byte to skip the filler
   i++;
   set_struct_fields(field, names[0], names[1], names[2], names[3],
		     names[4], names[5]);
   (field.charsetnr, field.length, field.type, field.flags, field.decimals) =
     unpack("x${i} <J <K C <J C xx"$, response);
   return field;
}

private define read_field_packets(dbh, field_count)
{
   variable metadata = Struct_Type[field_count], j;
   _for j (0, field_count - 1 , 1)
     {
	metadata[j] = parse_field_packet(read_packet(dbh.socket));
     }
   if (field_count)
     {
	read_eof_packet(dbh.socket);
     }
   return metadata;
}

private define handle_ok(dbh, response)
{
   variable server_status, i = 1, len;
   dbh._affected_rows = parse_lcb(response, i, &len);
   i += len;
   dbh._insert_id = parse_lcb(response, i, &len);
   i += len;
   dbh._server_status = unpack("x${i}<J"$, response);
}

%}}}
%{{{ mysql commands
%{{{  select db

private define select_db(dbh, db)
{
   write_packet(dbh.socket, pack(sprintf("C s%d", strbytelen(db)), COM_INIT_DB, db));
   () = read_packet(dbh.socket; expect = 0);
}

%}}}
%{{{  query

private define query(dbh, q)
{
   write_packet(dbh.socket, pack(sprintf("C s%d", strbytelen(q)), COM_QUERY, q));
   variable response = read_packet(dbh.socket);
   ifnot(response[0])
     {
	handle_ok(dbh, response);
	return; % OK response, query returned no result
     }
   
   % We expect 5 or more packets:
   % (Result Set Header Packet)  the number of columns
   % (Field Packets)             column descriptors
   % (EOF Packet)                marker: end of Field Packets
   % (Row Data Packets)          row contents
   % (EOF Packet)                marker: end of Data Packets

   variable field_count = parse_lcb(response, 0);

   variable metadata = read_field_packets(dbh, field_count);
   variable row = 0, column;
   variable userdata = qualifier("userdata", NULL);
   if (userdata != NULL)
     {
	try
	  {
	     userdata.metadata_callback(metadata);
	  }
	catch AnyError:
	  {
	     read_up_to_eof(dbh.socket);
	     throw;
	  }
     }
   
   
   % read row data packets, up to an EOF packet
   if (userdata == NULL)
     {
	read_up_to_eof(dbh.socket);
	return;
     }
   
   variable callback=userdata.column_callback;
   variable i = 0, len;
   forever
     {
	response = read_packet(dbh.socket);
	if (response[0] == 0xfe)  % EOF
	  {
	     break;
	  }
	try
	  {
	     i = 0;
	     _for column (0, field_count - 1, 1)
	       {
		  @callback(userdata, row, column, parse_lcs(response, i, &len));
		  i += len;
	       }
	  }
	catch AnyError:
	  {
	     read_up_to_eof(dbh.socket);
	     throw;
	  }
	row++;
     }
}

private define query_struct_metadata_callback(userdata, metadata)
{
   variable field_names = array_map(String_Type, &get_struct_field, metadata, "name");
   userdata.rv = @Struct_Type(field_names);
   loop(length(field_names))
     {
	list_append(userdata.columns, {});
     }
}

private define query_struct_callback(userdata, row, column, val)
{
   list_append(userdata.columns[column], val);
}

private define query_struct(dbh, q)
{
   variable userdata = struct {rv = NULL,
      columns = {},
      metadata_callback = &query_struct_metadata_callback,
      column_callback = &query_struct_callback,
   };
   query(dbh, q ; userdata = userdata);
   if (userdata.rv == NULL) return NULL;
   loop(length(userdata.columns))
     {
	list_append(userdata.columns, list_to_array(list_pop(userdata.columns)));
     }
   set_struct_fields (userdata.rv, __push_list(userdata.columns));
   return userdata.rv;
}

private define query_list_of_assoc_metadata_callback(userdata, metadata)
{
   userdata.field_names = array_map(String_Type, &get_struct_field, metadata, "name");
}

private define query_list_of_assoc_callback(userdata, row, column, val)
{
   if (row != userdata.row)
     {
	userdata.row = row;
	variable a = Assoc_Type[String_Type];
	userdata.a = a;
	list_append(userdata.rv, a);
     }
   userdata.a[userdata.field_names[column]] = val;
}

private define query_list_of_assoc(dbh, q)
{
   variable userdata = struct {rv = {}, a = NULL,
      row = NULL, field_names = NULL,
      metadata_callback = &query_list_of_assoc_metadata_callback,
      column_callback = &query_list_of_assoc_callback,
   };
   query(dbh, q ; userdata =  userdata);
   return userdata.rv;
}

%}}}
%{{{  prepared statement
%{{{   date/time structures

private variable timestruct = struct {
   tm_sec = 0,
   tm_min = 0,
   tm_hour = 0,
   tm_mday = 0,
   tm_mon = 0,
   tm_year = 0,
   tm_wday = 0,
   tm_yday = 0,
   tm_isdst = 0
};
private define parse_datetime(s, length)
{
   variable rv = @timestruct;
   % The documentation says
   % If all fields are 0, nothing is sent, but the length byte.
   % but in fact I get a one byte string 0x0A for some reason
   if(length < 4) return rv;
   
   (rv.tm_year, rv.tm_mon, rv.tm_mday) = unpack("<hCC", s);
   rv.tm_year -= 1900;
   rv.tm_mon--;
   if (length > 4)
     {
	rv.tm_hour = s[4];
	rv.tm_min = s[5];
	rv.tm_sec = s[6];
     }
   % Mysql does not provide values for wday, yday and dst
   return rv;
}

% We convert a FIELD_TYPE_TIME to a number of seconds. MySQL supports
% fractional times, but getting it to return a time value with
% fractional part as a FIELD_TYPE_TIME seems impossible. So for now
% this returns a long long int.

private define parse_time(s, length)
{
   ifnot(length) return 0;
   variable neg, days, hours, minutes, seconds, rv;
   (neg, days, hours, minutes, seconds) = unpack("c <K c c c", s);
   rv = days * 86400LL + hours * 3600 + minutes * 60 + seconds;
   return neg ? -1 * rv : rv;
}

private define datetime(s)
{
   ifnot(_is_struct_type(s)
	 && length(get_struct_field_names(s)) == length(get_struct_field_names(timestruct))
	 && NULL == wherefirst(get_struct_field_names(s) != get_struct_field_names(timestruct)))
     {
	throw RunTimeError, "not a time struct";
     }
   return lcs(pack("<h C5", s.tm_year + 1900, s.tm_mon + 1,
		   s.tm_mday, s.tm_hour, s.tm_min, s.tm_sec));
}

%}}}
%{{{   slang <-> mysql typemap

private variable typemap = struct 
{
   % The mtype is for mapping MySQL Types to S-Lang types. The sltype
   % is for mapping S-Lang types to MySQL Types. IOW MySQL string
   % types show up in S-Lang as bstrings, and S-Lang (b)strings show
   % up in MySQL as FIELD_TYPE_STRING.
   
   sltype = [Integer_Type, Long_Type, UInteger_Type, ULong_Type,
	     Char_Type, UChar_Type, Short_Type, UShort_Type,
	     LLong_Type, ULLong_Type, Float_Type, Double_Type,
	     Null_Type,
	     String_Type, String_Type, String_Type, String_Type,
	     BString_Type,
	     Float_Type, Struct_Type, LLong_Type, Struct_Type
	    ],

   mtype = [FIELD_TYPE_LONG, FIELD_TYPE_LONG, FIELD_TYPE_LONG, FIELD_TYPE_LONG,
	    FIELD_TYPE_TINY, FIELD_TYPE_TINY, FIELD_TYPE_SHORT, FIELD_TYPE_SHORT,
	    FIELD_TYPE_LONGLONG, FIELD_TYPE_LONGLONG, FIELD_TYPE_FLOAT, FIELD_TYPE_DOUBLE,
	    FIELD_TYPE_NULL,
	    FIELD_TYPE_STRING, FIELD_TYPE_VAR_STRING, FIELD_TYPE_VARCHAR, FIELD_TYPE_BLOB,
	    FIELD_TYPE_STRING,
	    FIELD_TYPE_NEWDECIMAL, FIELD_TYPE_DATETIME, FIELD_TYPE_DATE, FIELD_TYPE_TIME
	   ],
   
   unsigned = [0, 0, 1, 1,
	       0, 1, 0, 1,
	       0, 1, 0, 0,
	       0,
	       0, 0, 0, 0,
	       0,
	       0, 0, 0, 0
	      ],

   
   pack = ["<i", "<i", "<I", "<I",
	   "c", "C", "<h", "<H",
	   "<m", "<M", "d", "d",
	   NULL,
	   "", "", "", "",
	   "",
	   "", "", "", ""
	  ],

   size = [4, 4, 4, 4,
	   1, 1, 2, 2,
	   8, 8, 8, 8,
	   0,
	   0, 0, 0, 0,
	   0,
	   0, 0, 0, 0
	  ]
};

%}}}
%{{{   execute functions

private define execute()
{
   variable n = _NARGS - 1, parameters, stmt;
   if (n > 0)
     parameters = __pop_list(n);
   else
     parameters = {};
   stmt = ();
   variable userdata = qualifier("userdata", NULL);

   % send an execute packet
   
   variable null_bit_map, packet = pack("C <K x <K", COM_STMT_EXECUTE, stmt.statement_handler_id, 1),
     i = 0, val, param_types, values, t, response;
   if (n != stmt.parameter_columns)
     {
	throw RunTimeError, sprintf("stmt got %d parameters, should be %d", n, stmt.parameter_columns);
     }
   if (n > 0)
     {
	null_bit_map=Char_Type[(n+7)/8];
	foreach val (parameters)
	  {
	     if (val == NULL)
	       {
		  null_bit_map[i/8] |= (1 << (i mod 8));
	       }
	     i++;
	  }
	packet += pack(sprintf("C%d", (n+7)/8), null_bit_map);
	packet += pack("C", 1);  % new parameter bound flag.
	param_types = Char_Type[2*n];
	values = ""B;
	i = 0;
	foreach val (parameters)
	  {
	     t = wherefirst(typemap.sltype == typeof(val));
	     if (t == NULL)
	       {
		  throw MysqlClientError, "unsupported S-Lang type for parameter $i"$;
	       }
	     param_types[i*2] = typemap.mtype[t];
	     if (typemap.unsigned[t])
	       {
		  param_types[i*2+1] = 0x80;
	       }
	     if (typemap.pack[t] != NULL)
	       {
		  if (typemap.pack[t] == "")
		    {
		       if (typemap.sltype[t] == Struct_Type)
			 {
			    values += datetime(val);
			 }
		       else
			 {
 			    values += lcs(val);
			 }
		    }
		  else
		    {
		       values += pack(typemap.pack[t], val);
		    }
	       }
	     
	     i++;
	  }
	packet += pack(sprintf("C%d", 2 * n), param_types);
	packet += values;
     }
   else
     {
	packet += pack("C", 0);
     }
   write_packet(stmt.db.socket, packet);

   % read response
   
   response = read_packet(stmt.db.socket);
   ifnot(response[0])
     {
	handle_ok(stmt.db, packet);
	return;
     }
   
   variable column_count, unpack_format, metadata, field_info, unsigned, row_null_bitmap_length;
   
   % read metadata
   
   column_count = response[0];
   unpack_format = {};
   metadata = {};
   loop(column_count)
     {
	response = read_packet(stmt.db.socket);
	if (response[0] == 0xfe) throw MysqlError, "unexpected EOF";
	field_info = parse_field_packet(response);
	unsigned = field_info.flags & UNSIGNED_FLAG ? 1 : 0;
	n = wherefirst(typemap.mtype == field_info.type and typemap.unsigned == unsigned);
	if (n == NULL)
	  {
	     read_up_to_eof(stmt.db.socket);
	     read_up_to_eof(stmt.db.socket);
	     throw MysqlClientError, sprintf("Client does not support Mysql type %d, unsigned is %d", field_info.type, unsigned);
	  }
	list_append(metadata, field_info);
	list_append(unpack_format, n);
     }
   response = read_packet(stmt.db.socket; expect = 0xfe);
   row_null_bitmap_length = (column_count + 9) / 8;

   % read rows
   if (userdata == NULL)
     {
	read_up_to_eof(stmt.db.socket);
	return;
     }

   variable bit, decoder, offset, size, value, null_bit_map_format;

   userdata.metadata_callback(metadata);
   
   null_bit_map_format = "xC${row_null_bitmap_length}"$;
   forever
     {
	response = read_packet(stmt.db.socket);
	if (response[0] == 0xfe) return;

	null_bit_map = unpack(null_bit_map_format, response);
	bit = 4; % first two bits are reserved
	offset = 1 + row_null_bitmap_length;
	values = {};
	i = 0;
	foreach decoder(unpack_format)
	  {
	     if (null_bit_map[i] & bit)
	       {
		  list_append(values, NULL);
	       }
	     else
	       {
		  size = typemap.size[decoder];
		  switch (typemap.pack[decoder])
		    {
		     case NULL:
		       value = NULL;
		    }
		    {
		     case "":
		       value = parse_lcs(response, offset, &size);
		    }
		    {
		       value = unpack(typemap.pack[decoder], response[[offset : offset+size-1]]);
		    }
		  switch (typemap.mtype[decoder])
		    {
		     case FIELD_TYPE_DATE or case FIELD_TYPE_DATETIME:
		       value = parse_datetime(value, size);
		    }
		    {
		     case FIELD_TYPE_TIME:
		       value = parse_time(value, size);
		    }
		    {
		     case  FIELD_TYPE_NEWDECIMAL:
		       value = atof(value);
		    }
		  list_append(values, value);
		  offset += size;
	       }
	     bit = bit << 1;
	     ifnot(bit & 255)
	       {
		  i++;
		  bit = 1;
	       }
	  }
	userdata.row_callback(values);
     }
}

private define execute_list_of_struct_metadata_callback(userdata, metadata)
{
   variable field_names = String_Type[length(metadata)];
   variable i;
   _for i (0, length(metadata) - 1, 1)
     {
	field_names[i] = metadata[i].name;
     }
   userdata.field_names = field_names;

}

private define execute_list_of_struct_callback(userdata, rowdata)
{
   variable field_names = userdata.field_names;
   variable rv = @Struct_Type(field_names);
   set_struct_fields(rv, __push_list(rowdata));
   list_append(userdata.rv, rv);
}

private define execute_list_of_struct()
{
   variable args = __pop_list(_NARGS);
   variable userdata = struct
     {
	field_names = NULL,
	rv = {},
	metadata_callback = &execute_list_of_struct_metadata_callback,
	row_callback = &execute_list_of_struct_callback,
     };
   execute(__push_list(args) ; userdata = userdata);
   return userdata.rv;
}


private define execute_list_of_assoc_callback(userdata, rowdata)
{
   variable name, rv = Assoc_Type[Any_Type];
   foreach name (userdata.field_names)
     {
	rv[name] = list_pop(rowdata);
     }
   list_append(userdata.rv, rv);
}

private define execute_list_of_assoc()
{
   variable args = __pop_list(_NARGS);
   variable userdata = struct
     {
	field_names = NULL,
	rv = {},
	metadata_callback = &execute_list_of_struct_metadata_callback,
	row_callback = &execute_list_of_assoc_callback,
     };
   execute(__push_list(args) ; userdata = userdata);
   return userdata.rv;
}

private define execute_list_of_list_metadata_callback(userdata, metadata)
{
}

private define execute_list_of_list_callback(userdata, rowdata)
{
   list_append(userdata.rv, rowdata);
}

private define execute_list_of_list()
{
   variable args = __pop_list(_NARGS);
   variable userdata = struct
     {
	rv = {},
	row_callback = &execute_list_of_list_callback,
	metadata_callback = &execute_list_of_list_metadata_callback,
     };
   execute(__push_list(args) ; userdata = userdata);
   return userdata.rv;
}

%}}}
%{{{   stmt object

private define destroy_stmt(stmt)
{
   write_packet(stmt.db.socket, pack("C <K", COM_STMT_CLOSE, stmt.statement_handler_id));
}

private define prepare(dbh, query)
{
   write_packet(dbh.socket, pack(sprintf("C s%d", strbytelen(query)), COM_STMT_PREPARE, query));
   variable response = read_packet(dbh.socket; expect = 0);
   variable statement_handler_id, field_count, param_count;
   (statement_handler_id, field_count, param_count) = unpack("x <K <J <J", response);
   
   loop(param_count)
     {
	% parameter packets are not used yet
	()=read_packet(dbh.socket);
     }
   if (param_count)
     read_eof_packet(dbh.socket);

   variable field_info = read_field_packets(dbh, field_count);
   variable stmt = struct
     {
	db = dbh,
	statement_handler_id = statement_handler_id,
	result_columns = field_count,
	parameter_columns = param_count,
	fields = field_info,
	execute = &execute,
	execute_list_of_struct = &execute_list_of_struct,
	execute_list_of_assoc = &execute_list_of_assoc,
	execute_list_of_list = &execute_list_of_list,
     };
   __add_destroy(stmt, &destroy_stmt);
   return stmt;
}

%}}}
%}}}
%{{{  escape parameter

% Escape quotes when NO_BACKSLASH_ESCAPES is in effect. This will
% truncate strings with NULLs, but since these are not valid as SQL
% code and perl DBD::Mysql's quote() method has the same behavior I
% think that's OK.

private define escape_quotes(text)
{
   return str_quote_string(text, "'", '\'');
}

private define make_encode_table()
{
   variable table = array_map(String_Type, &char, [0:255]);
   table[0] = "\\0";
   table['\b'] = "\\b";
   table['\t'] = "\\t";
   table['\r'] = "\\r";
   table['\n'] = "\\n";
   table['\''] = "\\'";
   table['"'] = "\\\"";
   table['\\'] = "\\\\";
   return table;
}

private variable encode_table = NULL;

% Escape a parameter. We only need to support ASCII and UTF-8.
private define escape(dbh, text)
{
   if (dbh.server_status() & SERVER_STATUS_NO_BACKSLASH_ESCAPES)
     return escape_quotes(text);   
   if (encode_table == NULL)
     encode_table = make_encode_table();
   return strjoin(encode_table[bstring_to_array(text)], "");
}

%}}}
%}}}
%{{{ mysql object
private define affected_rows(dbh)
{
   return dbh._affected_rows;
}

private define insert_id(dbh)
{
   return dbh._insert_id;
}

private define server_status(dbh)
{
   return dbh._server_status;
}

private define destructor(dbh)
{
   write_packet(dbh.socket, pack("C", COM_QUIT));
}

private define destroy_mysql(dbh)
{
   if (dbh.destructor != NULL)
     dbh.destructor();
}

define mysql_connect(host, user, password)
{
   % connect
   
   variable dbh = struct {
      socket = mysql_socket_new(host, qualifier("port", 3306)),
      select_db = &select_db,
      query = &query,
      query_struct = &query_struct,
      query_list_of_assoc = &query_list_of_assoc,
      prepare = &prepare,
      escape = &escape,
      destructor = &destructor,
      affected_rows = &affected_rows,
      _affected_rows = 0,
      insert_id = &insert_id,
      _insert_id = 0,
      _server_status,
      server_status = &server_status,
   };
   __add_destroy(dbh, &destroy_mysql);

   % get handshake packet
   
   variable protocol_version, server_version, thread_id, scramble_buf, server_capabilities,
     server_language, scramble_buffer, scramble_buffer2;
   variable packet = read_packet(dbh.socket);
   (protocol_version, server_version, thread_id, scramble_buffer,
       server_capabilities, server_language, dbh._server_status,
       scramble_buffer2
   ) = unpack(sprintf("C s%d x <K s8 x <J C <J x13 s12 x", strbytelen(packet) -1), packet);

   % login
   
   variable client_flags, max_packet_size;
   client_flags = CLIENT_PROTOCOL_41 | CLIENT_SECURE_CONNECTION | CLIENT_CONNECT_WITH_DB;
   variable charset = _slang_utf8_ok ? 200 : 8;
   max_packet_size = 4093; % This is ignored by the server I believe
   variable database = qualifier("database", "");
   if (password == NULL)
     {
	packet = pack(sprintf("<K <K C x23 s%d x x s%d x", strbytelen(user), strbytelen(database)),
		      client_flags, max_packet_size, charset, user, database);
     }
   else
     {
	variable scramblebuf = scramble41(password, scramble_buffer + scramble_buffer2);
	variable scramblelen = bstrlen(scramblebuf);
	packet = pack(sprintf("<K <K C x23 s%d x C s%d s%d x", strbytelen(user),
			      scramblelen, strbytelen(database)),
		      client_flags, max_packet_size, charset, user,
		      scramblelen, scramblebuf, database);
     }
   write_packet(dbh.socket, packet; sequence=1);
   () = read_packet(dbh.socket; expect = 0);
   return dbh;
}

%}}}
provide("mysqlclient");
